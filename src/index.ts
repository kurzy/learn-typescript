let number = '42';
// number = 42;

let language: string;
language = "TypeScript";
language = "JavaScript";
// language = 42;


const numbers: Array<number> = [];
numbers.push(3.14, 42, 1.161);
// numbers.push(language);

let isTowelDay: boolean | string = false;
isTowelDay = 'Eště ne';
// isTowelDay = 5;

const hitchhaker: Array<string | number | boolean> = ["Arthur Dent", 42, !!isTowelDay];

type THitchhikerValue = string | number | boolean;
export type THitchhaker = Array<THitchhikerValue>;

const arthur: THitchhaker = ["Arthur", 58, false];

// tuplet
type TActor = [name: string, age?: number, isAlive?: boolean];
const harryPoter: TActor = ["Daniel Radcife", , true];

const color: {
  redChannel: number;
  greenChannel: number;
  blueChannel: number;
  name?: string;
} = {
  redChannel: 255,
  blueChannel: 0,
  greenChannel: 25,
  name: undefined,
};

type TColor = {
  redChannel: number;
  greenChannel: number;
  blueChannel: number;
  name?: string;
};

let yellowColor: TColor = {} as TColor;
yellowColor.blueChannel = 0;
yellowColor.greenChannel = 255;
yellowColor.redChannel = 255;
yellowColor.name = 'yellow';

let anotherColor: TColor = {
  blueChannel: 0,
  greenChannel: 255,
  redChannel: 255,
};

console.log(anotherColor);

type TSinger = Record<string, string | number | boolean>;

const kaja: TSinger = {
  name: 'Karel Gott',
  age: 80,
  isAlive: true,
};

for (let key of Object.keys(kaja)) {
  console.log(kaja[key]);
}

type TColorKeys = 'red' | 'green' | 'blue' | 'name';
type TColorRecord = Record<TColorKeys, number | string>;
const pink: TColorRecord = {
  red: 255,
  green: 200,
  blue: 200,
  name: 'pink',
};

console.log(pink);

interface IHuman {
  name: string;
}

interface IHuman {
  age: number;
}

const human: IHuman = {
  name: 'Já 1.',
  age: 51,
};

type THumanName = {
  name: string;
};
type THumanAge = {
  age: number;
};

type THumanNameAge = THumanName & THumanAge;

const me1: THumanNameAge = {
  name: "Vilém",
  age: 51,
};


interface IHumanName {
  name: string;
}
interface IHumanAge {
  age: number;
}

type THumanAgeName = IHumanAge & IHumanName;

const me2: THumanAgeName = {
  name: "Vilém",
  age: 51,
};

type TUnionHuman = THumanAge | THumanName;

const anyHuman: TUnionHuman = {
  age: 80,
  name: 'Karel',
};

/*
interface IUnionHuman = IHumanAge | IHumanName;
const anyHuman2: IUnionHuman = {
  // age: 80,
  name: 'Karel',
};
*/

interface IExtendedHuman extends IHumanAge, IHumanName {
  car?: string;
}

const anyHuman2: IExtendedHuman = {
  age: 80,
  name: 'Karel',
  car: 'rols',
};

const icons = {
  pivo: './beer.svg',
  limo: './lemonade.svg',
  rum: './metuzalem.svg',
  slivka: './bilakoralka.svg',
};

type TIcons = typeof icons;
type TIconType = keyof TIcons;

const getIcon = (icon: TIconType) => icons[icon];

console.log(getIcon('pivo'));
// console.log(getIcon('slivovice'));

type TCar = {
  carType: string;
  color: string;
  wheelCount: number;
};

const getCar = (carType: string, color: string): TCar => ({
  carType, color, wheelCount: 4,
});

const myCar: TCar = getCar('Pažout', 'blue');

console.log(myCar.wheelCount);

type TArrayForCount = Array<number | string>;
const numbersAndStrings: TArrayForCount = [1, 2, 'Karel', 25, 3, 'Luděk', 'Dr. Voštěp', 42, 3.14];

type TSumObject = {
  numberCount: number;
  stringCount: number;
};

const sumObject: TSumObject = numbersAndStrings.reduce((reducer: TSumObject, current): TSumObject => {
  let { numberCount, stringCount } = reducer;

  switch (typeof current) {
    case 'number':
      numberCount++;
      break;
    case 'string':
      stringCount++;
      break;
    default:
  }

  return {
    numberCount, stringCount
  };
}, {
  numberCount: 0,
  stringCount: 0,
});

console.log(sumObject);

type TSumFnc = (a: number, b: number) => number;

type TBetterSumObject = {
  a: number;
  b: number;
  sum: TSumFnc;
};

const betterSumObj: TBetterSumObject = {
  a: 20,
  b: 22,
  sum: (a, b) => a + b,
};

const mySum = betterSumObj.sum(betterSumObj.a, betterSumObj.b);
console.log(mySum);

const x = (): void => {
  return;
};

// const getAny = (x) => x;

enum EChlast {
  PIVO = 200,
  RUM = 404,
  ZELENA,
  SLIVOVICE,
  JAGER,
  BECHEROVKA,
}

console.log(EChlast.PIVO);
console.log(EChlast.RUM);

const getChlast = (chlast: EChlast): string => chlast.toString();
console.log(getChlast(EChlast.SLIVOVICE));
console.log(Object.keys(EChlast));

type TGenPerson = {
  name: string;
  surname: string;
};

type TAnimal = {
  name: string;
  type: string;
};

type TPartialReturn = {
  id: number;
};

const addId = <T>(item: T): T & TPartialReturn => {
  const id = Math.ceil(Math.random() * 1e10);
  return { ...item, id };
};

const person = addId<TGenPerson>({ name: 'John', surname: 'Doe' });
console.log(person.id);
console.log(person.name);

const cat = addId<TAnimal>({ name: 'Micinka', type: 'cat' });
console.log(cat.id);
console.log(cat.name);

const removeUndefined = <T>(array: Array<T | undefined>): Array<T> => {
  const filterFunction = (value: T | undefined): value is T => value !== undefined;
  const filteredValues = array.filter(filterFunction);
  return filteredValues;
}


const numbersWithUndefined: Array<number | undefined> = [1, 2, 3, undefined, 5, undefined, 7];
console.log(removeUndefined<number>(numbersWithUndefined));

interface ITodo {
  title: string;
  completed: boolean;
  completedDate: Date;
}

interface IPartialTodo extends Omit<ITodo, 'completedDate'> {
  completedDate?: ITodo['completedDate'];
}

const todo: IPartialTodo = {
  title: 'Jít na Frontendisty',
  completed: false,
};

console.log(todo);

type TBEArray = Array<string>;
const item: TBEArray[number] = '42';

type TGCh = ReturnType<typeof getChlast>;
const piti: TGCh = 'bumbani';

interface IPingable {
  ping(): void;
}
interface IPongable {
  pong(): void;
}

class PingPong implements IPingable, IPongable {
  pong() {
    console.log('pong');
  }
  ping() {
    console.log('ping');
  }
}

const pp = new PingPong();
// pp.ping();
pp.pong();

class Spp extends PingPong {
  pingpong(): void {
    this.ping();
    this.pong();
  }
}

const spp = new Spp();
spp.pingpong();

type T0 = NonNullable<string | number | null>;
const consoluj = (x: T0): void => console.log(x);

// consoluj(null);
